package at.kolb.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor implements Actor {
	private int rectX,rectY;
	private boolean rectUp = false;
	private boolean rectDown = false;
	private boolean rectLeft = false;
	private boolean rectRight = true;

	
	
	public RectActor(int rectX, int rectY) {
		super();
		this.rectX = rectX;
		this.rectY = rectY;
	}

	public void render(Graphics graphics) {
		graphics.drawRect(this.rectX, this.rectY, 50, 50);
		
		
	}
	
	public void update(GameContainer gc, int delta) {
		if (this.rectX < 700 && this.rectRight == true) {
			this.rectX++;
			if (this.rectX == 700) {
				this.rectRight = false;
				this.rectDown = true;
			} else {
				this.rectRight = true;

			}

		} 
		else if (this.rectY < 500 && this.rectDown == true) {
			this.rectY++;
			if (this.rectY == 500) {
				this.rectDown = false;
				this.rectLeft = true;
			} else {
				this.rectDown = true;
			}
		} 
		else if (this.rectX > 55 && this.rectLeft == true) {
			this.rectX--;
			if (this.rectX == 55) {
				this.rectLeft = false;
				this.rectUp = true;
			} else {
				this.rectLeft = true;
			}

		} 
		else if (this.rectY > 50 && this.rectUp == true) {
			this.rectY--;
			if (this.rectY == 50) {
				this.rectUp = false;
				this.rectRight = true;
			} else {
				this.rectUp = true;
			}

		}
		
		
	}
	
	
}
