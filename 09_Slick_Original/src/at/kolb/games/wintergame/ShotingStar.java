package at.kolb.games.wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ShotingStar implements Actor {
	private float x,y;
	private float time;
	private Random random;
	private float counter;
	private float start;
	private boolean isVisible;
	

	
	public ShotingStar() {
		this.x = x;
		this.y = y;
		this.random= new Random();
		this.counter = counter; 
		this.isVisible = false;
		setRandomTime();
		setRandomXYValues();
	}
	

	
	private void setRandomTime() {
		this.time = this.random.nextInt(10000);
	}
	
	private void setRandomXYValues() {
		this.x = 0;
		this.y = (float) -100;//this.random.nextInt(0);
		this.start = this.random.nextInt(200);
	}
	
	public void render(Graphics graphics) {
		graphics.setColor(Color.yellow);
		if(this.isVisible == true) {
		graphics.fillOval(this.x, this.y, 20, 20, 4);
		}
		graphics.setColor(Color.white);
	}

	public void update(GameContainer gc, int delta) {
		this.counter++;

		if (this.counter < this.time) {
			this.isVisible = true;
			this.x++;
			this.y = (float) (0.0005*this.x*this.x+-0.001*this.x+this.start);
		}
		
		/*else if (this.counter < this.time && this.x >= 400) {
			this.isVisible = true;
			this.x++;
			this.y = this.y + (float) 0.5;
		}
	*/
		else {
			setRandomXYValues();
			setRandomTime();
			this.isVisible = false;
			this.counter = 0;
		}
	
		
	}
	

}
