package at.kolb.games.wintergame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;


public class Snowflakes implements Actor {
	private float x,y;
	private float size;
	private float speed;
	private Random random;
	
	public Snowflakes(String size) {
		super();
		this.random = new Random();
		setRandomXYValues();
		if(size.equals("big")) {
		this.size = 25;	
		this.speed = (float) 1;
		}
		if(size.equals("medium")) {
		this.size = 15;	
		this.speed = (float) 0.75;
		}
		if(size.equals("small")) {
		this.size = 7;	
		this.speed = (float) 0.5;
		}
	
	}
	
	private void setRandomXYValues() {
		this.x = this.random.nextInt(800);
		this.y = this.random.nextInt(600)*-1;
	}
	
	
	
	public void render(Graphics graphics) {
	//	graphics.drawOval(this.flakeX, this.flakeY, 50, 50);
		graphics.fillOval((float)this.x, (float)this.y, size, size);
	}

	public void update(GameContainer gc, int delta) {
		if (this.y < 700) {
			this.y = this.y + this.speed;
		} else {
		setRandomXYValues();
		}
		
	}



}

