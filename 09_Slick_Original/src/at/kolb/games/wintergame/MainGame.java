package at.kolb.games.wintergame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;
import java.util.ArrayList;
import java.util.List;

public class MainGame extends BasicGame {

	private List<Actor> actors;
	
	MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		/*rect1.render(graphics);
		circle1.render(graphics);
		oval1.render(graphics);
		*/
		for (Actor actors : this.actors) {
			actors.render(graphics);
		}

	
	}

	@Override
	public void init(GameContainer gc) throws SlickException {

		/*rect1 = new RectActor(100,100);
		circle1 = new CircleActor(370,250);
		oval1 = new OvalActor(700,500);
		*/
		
		this.actors = new ArrayList<>();
		this.actors.add(new ShotingStar());

		for(int i = 0; i<50; i++){
			this.actors.add(new Snowflakes("big"));
			this.actors.add(new Snowflakes("small"));
			this.actors.add(new Snowflakes("medium"));
		}
		this.actors.add(new Snowman());
	}

	@Override
	public void update(GameContainer gc, int delta){

		/*rect1.update(gc, delta);
		circle1.update(gc, delta);
		oval1.update(gc, delta);
		*/
		for (Actor actors : this.actors) {
			actors.update(gc,delta);
		}


	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
