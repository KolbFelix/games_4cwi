package at.kolb.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor implements Actor {
	private int circleX, circleY;
	
	
	public CircleActor(int circleX, int circleY) {
		super();
		this.circleX = circleX;
		this.circleY = circleY;
	}

	public void render(Graphics graphics) {
		graphics.drawOval(this.circleX, this.circleY, 50, 50);
		
	}
	
	public void update(GameContainer gc, int delta) {
		if (this.circleY < 700) {
			this.circleY++;
		} else {
			this.circleY = 0;
		}
		
	}
}
