package at.kolb.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor implements Actor {
	private int ovalX, ovalY;
	private boolean ovalRight = false;
	private boolean ovalLeft = true;
	public OvalActor(int ovalX, int ovalY) {
		super();
		this.ovalX = ovalX;
		this.ovalY = ovalY;
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval(this.ovalX, this.ovalY, 70, 50);
	}

	public void update(GameContainer gc, int delta) {
		if (this.ovalX > 50 && this.ovalLeft == true) {
			this.ovalX--;
			if (this.ovalX==50) {
				this.ovalLeft = false;
				this.ovalRight = true;
			}
			else {
				this.ovalLeft = true;
			}
			
		}
		else if (this.ovalX < 700 && this.ovalRight) {
			this.ovalX++;
			if (this.ovalX==700) {
				this.ovalRight = false;
				this.ovalLeft = true;
			}
			else {
				this.ovalRight = true;
			}
			
		}
	}
}
