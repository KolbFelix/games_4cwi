package at.kolb.games.wintergame;

import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowman implements Actor {
	private float x,y;
	private float ballX,ballY;
	private Image image;
	private boolean isVisible;
	private boolean keySpace;
	private boolean go;
	private boolean keyescape, keydown, keyup, keyRight, keyLeft;
	
	

	

Snowman() throws SlickException{
	    this.image = new Image("testdata/Snowman.png");
	  	this.x = x;
		this.y = y;
		this.ballX = ballX;
		this.ballY = ballY;
		this.image = image;
		this.isVisible = false;
		this.keySpace = false;
		this.keyLeft = false;
		this.keyRight = false;
		this.go = false;
	    }

	@Override
	public void update(GameContainer gc, int delta) {
		this.keyescape = gc.getInput().isKeyDown(Input.KEY_ESCAPE); 
	    this.keyup = gc.getInput().isKeyDown(Input.KEY_UP);
	    this.keydown = gc.getInput().isKeyDown(Input.KEY_DOWN);
	    this.keyRight = gc.getInput().isKeyDown(Input.KEY_RIGHT);
	    this.keyLeft = gc.getInput().isKeyDown(Input.KEY_LEFT);
	    if (this.keydown == true) {
	       this.y++;
	    }
	    else if (this.keyup == true) {
	        this.y--;
	    }
	    else if (this.keyRight == true) {
	        this.x++;
	    }
	    else if (this.keyLeft == true) {
	        this.x--;
	    }
	    
	     else if (this.keyescape == true) {
	        System.exit(0);
	     }
		this.keySpace = gc.getInput().isKeyDown(Input.KEY_SPACE);
		if (keySpace == true) {
			isVisible = true;
			go = true;
			this.ballX = this.x + 22;
			this.ballY = this.y + 15;
		}
		if (go == true && this.ballX<900) {
			this.ballX++;
		}
		else if(this.ballX>=900){
			go = false;
			isVisible = false;
		}
		
	
	}

	@Override
	public void render(Graphics graphics) {
		if(isVisible == true) {
			graphics.fillOval(this.ballX, this.ballY, 20, 20);
		}
		graphics.drawImage(image, this.x, this.y);		
	}
	
	
}